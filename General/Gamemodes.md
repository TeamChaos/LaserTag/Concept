# Gamemodes


## Overview
___________________
### Must have
- TDM
- Free for all

### Nice to have
- Gungame (free for all)
- Conquest (Assault)
- Capture the Flag

### Maybe
- Rush
- Squad Deathmatch

## Details
___________________
### TDM
- 2 Teams
- Am besten zwei Respawn Punkte
- Spiel endet nach Zeit oder nach Anzahl Abschüssen

###### Offene Probleme
*Respawns*  
Möglichkeit 1  
- Feste Respawn Punkte
- Abgeschossene müssen hier hin
- Nach Respawn erst Aktivierung der Waffe dann der Empfänger
- Dadurch gegnerische Respawn Punkte "Todeszone"

Möglichkeit 2  
- Respawn nach Timeout
- Zuerst Aktivierung der Weste dann der Waffe
- Spieler müssen sich "sicheres" Gebiet zum respawnen suchen
- Eventuell mit manuellen Trigger nach respawn



###### Benötigte Levelelemente
- Eventuell zwei Respawn Positionen pro Team

###### Potentiell relevante Informationen
- Abschüsse pro Spieler
- Tode pro Spieler
- Assists
- Team Zugehörigkeit


### Free for all
- Jeder kann jeden treffen
- Respawn an einem zentralen Punkt oder nach Zeit oder gar nicht (Battle Royale)
- Spiel endet nach Zeit oder bei bestimmter Anzahl Abschüsse durch eine Person

###### Offene Probleme
*Respawns*  
Möglichkeit 1
- Battle Royal -> Keine respawns


Möglichkeit 2  
- Respawn nach Timeout
- Zuerst Aktivierung der Weste dann der Waffe
- Spieler müssen sich "sicheres" Gebiet zum respawnen suchen
- Eventuell mit manuellen Trigger zum respawn

###### Benötigte Levelelemente
 Keine
###### Potentiell relevante Informationen
- Abschüsse pro Spieler
- Tode pro Spieler
- Assists

### Gungame
- Für jeden Abschuss (oder alle zwei) bekommt man ein permanentes Weapon Upgrade (oder Downgrade)
- Sonst wie Free For All

###### Offene Probleme
*Respawns*  
Möglichkeit 1
- Respawn nach Timeout
- Zuerst Aktivierung der Weste dann der Waffe
- Spieler müssen sich "sicheres" Gebiet zum respawnen suchen
- Eventuell mit manuellen Trigger zum respawn

###### Benötigte Levelelemente
Keine

###### Potentiell relevante Informationen
- Wie Free For All


### Conquest
- Zwei Teams
- Flaggenpunkte müssen eingenommen werden
- Eingenommene Flaggenpunkte dienen als Spawnpunkte?
- Teampunkte werden aus Zeit x gehaltenen Flaggen + Abschüsse berechnet
- Im Assault Modus ausschließlich Flaggen spawns somit Niederlage wenn alle Flaggen eingenommen und niemand mehr am Leben

###### Offene Probleme
*Respawns an Flaggen*  
Möglichkeit 1
- Respawn nach Timeout
- Zuerst Aktivierung der Weste dann der Waffe
- Spieler müssen sich "sicheres" Gebiet zum respawnen suchen
- Eventuell mit manuellen Trigger zum respawn

###### Benötigte Levelelemente
- Flaggenpunkte (connected)
- 1-2 Spawnpunkte

###### Potentiell relevante Informationen
- Abschüsse pro Spieler
- Tode pro Spieler
- Teamzugehörigkeit
- Flaggenstatus
- (Indirekt) Alive status

### Capture the Flag
- Zwei Teams
- Ein Flaggenpunkt + Flagge pro Team
- Feindliche Flagge muss zum eigenen Flaggenpunkt gebracht werden während die eigene Flagge anwesend ist

###### Offene Probleme
*Respawns*  
Möglichkeit 1
- Respawn an eigener Flagge nach Timeout
- Zuerst Aktivierung der Weste dann der Waffe
- Spieler müssen sich "sicheres" Gebiet zum respawnen suchen
- Eventuell mit manuellen Trigger zum respawn

*Flaggenzählung*  
- Wie erkennen ob eigene Flagge auch da
- Wie zählen der geklauten Flagge
- Wie zurückbringen der geklauten Flagge (eventuell einfach mehrere)

###### Levelelemente
- Zwei Flaggenpunkte
- Zwei (oder mehrere) stehlbare Flaggen

###### Potentiell relevante Informationen
- Abschüsse pro Spieler
- Tode pro Spieler
- Teamzugehörigkeit
- Anzahl der Flaggendiebstähle

### Rush
- Zwei Teams - Verteidiger/Angreifer
- Mehrere Flaggen o.ä. die nacheinander eingenommen werden müssen
- Begrenzte Respawns auf Angreifer Seite

###### Probleme

###### Levelelemente
- Zahlreiche Angriffspunkte (pro Abschnitt 1+)
- Zahlreiche Spawnpunkte (pro Abschnitt 1)

###### Potentiell relevante Informationen
- Abschüsse pro Spieler
- Tode pro Spieler
- Teamzugehörigkeit
- Flaggenstatus
- Übrige Respawntickets

### Squad Deathmatch
- Wie TDM aber mit mehreren kleineren Teams



## Required (transmitted) information
___________________


### Notation
Zusammenfassung aller benötigter Informationen die übertragen werden müssen.
In Kursiv Dinge die zeitkritisch sind.

s2x - Spieler an alle  
s2m - Spieler an Master  
s2s - Spieler zu einem Spieler  
b2m - Basis/Flagge an Master  
usw.  

### Zu Beginn/Ende
- Teamzugehörigkeit (m2x)
- Tode pro Spieler (s2m)
- Abschüsse pro Spieler (s2m)
- Assists pro Spieler (s2m)


### Zwingend erforderlich während des Spiels
- Tode pro Spieler (s2m)
- _Spielstatus_ (m2s)
- Flaggenstatus (b2m) (b2s || m2s)


### Nice to have
- _Einzelner Abschuss_ (s2s)
- Kills pro Spieler (s2m + m2s)
- Übrige Respawn Tickets (m2s)

