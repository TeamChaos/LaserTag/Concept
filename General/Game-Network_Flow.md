# Game/Network Flow
General sequence of game states.  
Required/used tranmission channels and transmitted information.

## Sequence 
### Setup Phase
- Turn on Base
    - Wifi Enable  
- Turn on Player ESP
    - Connect to Base (Wifi)
    - Sign-in/transmit id (HTTP/Wifi)
- Configure Base (HTTP/Wifi)
    - Setup teams
    - Configure gamemode
- _Once ready_ Send game ready signal from base to Player ESP (433MHz)
    - Player ESP request game information from base (HTTP/Wifi)
    - Player ESP send ready signal to base (HTTP/Wifi)
    - Player ESP disconnect from Base (Wifi)

###  Game Phase
- Player ESP setup Wifi access point for smartphone app
- Base sends game start signal (433MHz)
- During game in parallel:
    - Shot transmissions  - player to player (IR)
    - Heal/Resupply/Respawn/Gamemode related transmissions - level objects to player (IR)
    - Gamemode related transmissions - player to level objects (IR)
    - Death messages - player to all (player/base) (433MHz)
    - Level updates e.g. captured flag points - level object to base (433MHz)
    - Game updates - base to players and level updates (433MHz)
    - Local scoreboard - player ESP to player app (Wifi)
- Base sends game stop signal to player ESPs (433MHz)
- Player ESP stops wifi access point (Wifi)

### End Phase
- Player ESPs try to connect to base (Wifi) 
- Player ESPs send verified (locally collected) information to base (HTTP/Wifi)
- Base evaluates results and presents final result

### Restart
- Base resets game status
- ESPs are rebooted  
_or_  
- ESPs frequently query a certain location for updates (HTTP/Wifi)  
_or_  
- ESPs listen on certain port and base queries all previously registered ESPs (HTTP/Wifi)  
_or_  
- Full reset signal (433MHz)


## Intentional and Unintentional Interrupts
- Complete reset by just rebooting everything
- ESP looses power
    - could store game status in EEPROM and continue where left off
    - requires reset button at ESP to force reset
- Abort game
    - Via base Wifi/Webserver
    - Send stop/reset signal (433MHz)
- ESP misses important 433MHz message
    - ESP does not receive game ready messages
        - Unlikely
    - ESP does not receive game start messages
    - ESP does not receive game stop message
    - Could use button (combined with status LED) to manually trigger certain events
    - Could use app for complete control over ESP