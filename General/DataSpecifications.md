# General Data Specifications

This document is meant to describe data structures used in many parts of the projects.

### Health/Damage

Healt and damage are defined as integer values.

The default range is 0 to 100 where 100 reflects full health and 0 means dead.
In future versions special effects might allow for values higher than 100.

### Player/Thing Identifier

Players and things (level elements like medkits) are identified using a 5 bit ID.

The MSB is reserved for separating players (1) and things (0), the remaining 4 bits can be choosen freely.

### Additional Data

Every IR transmission contains an additional set of 5 bits. The meaning of these depends on the player/thing. For medkits or players (shots) this might relate to health/damage. This could also be used as a sub identifier.

#### Player/Shots

The data value describes the damage inflicted. 

The LSB is equal to 4 damage (which allows for a range between 4 and 120).

Notwithstanding the above, the `0b11111` and `0b00000` have a different meaning:

The maximum value (`0b11111`) is defined as infinite damage.
The minimum value (`0b00000`) is defined as 1 damage.

### Medkits

The data value describes the health healed.

The LSB is equal to 4 health (which allows for a range between 4 and 120).

Notwithstanding the above, the `0b11111` and `0b00000` have a different meaning:

The maximum value (`0b11111`) is defined as infinite health.
The minimum value (`0b00000`) is defined as 1 health.