# I2C Bus

### Assigned IDs

- 0x10: Detector
- 0x11: Weapon
- 0x12: PortExpander
- 0x14: LED Driver

### Reserved IDs

- 0x00: General Call
- 0x70: LED Driver Catch All
- 0x03: LED Driver Reset