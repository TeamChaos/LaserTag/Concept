# Detector Control
## General
The detector is controlled via I2C. Therefore the communication is always initiated by the master (PMD). However, the detector has an additional single pin connection line to the master which can be used to indicate a received IR signal and therby requesting the master to initiate a communication.  

Due to the nature of I2C, the master always starts by addressing the client with its 7bit ID and saying whether it wants to read or write something. Then either the slave or the master send their bytewise.  
  
If the master wants to read a specific byte it first writes a single byte to the client which is used as address pointer for the next read.

## Our setup
For the purpose of controlling the detector there are three possible kind of communication:
- Change detector status (detect/paused)
- Get decoded IR signal (once transmit pin is high)
- Get a single status byte (error counter or status)

Any 0xFF byte sent by the detector is interpreted as an error signal, therfore it must not occur in normal transmissions.  
You can find all address bytes in CommonLib/detector_control/protocol.h

### Control status
To control the status the master simply writes the ID/address byte of the status register (_IC_STATUS_SET_) followed by the status to set.

### Get decoded IR signal
The decoded IR signal is only available if the transmit pin is high. While the transmit pin is high the detector is paused and cannot detect any more IR signals, therefore it is important to quickly read the decoded signal.  
To get the decoded signal the master first writes the ID/address byte _I2C_GET_ALL_ to set the slaves address pointer. Then it reads seven bytes continuously (by sending ACKs instead of NACKS [which terminate the connection]). The first and the last byte have defined values (_I2C_ALL_OK_) which can be used to verify the connection is working properly.  
Once the last byte has been read the detector automatically switches to the detecing state again and sets the transmit pin low.

### Get status 
To get a single (status) byte, the master simply writes the address byte and then reads a single byte back.

## The decoded signal
The decoded signal consists of several bytes:
1. Decoded player flag (f) and id (i) and the byte parity bit (p) 0bp00fiiii
2. Decoded data(d) and the byte parity bit (p) 0bp00ddddd
3. Receiver status bits one: One bit corresponds to one receiver. 1 means it received a message - LSB: Receiver 1, 2nd-MSB: Receiver 7, MSB: 0
4. Receiver status bits two: One bit corresponds to one receiver. 1 means it received a message - LSB: Receiver 8