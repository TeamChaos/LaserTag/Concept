 # IR Protocol

The IR transmission is based on ON_OFF_Keying modulation ontop of a 38KHz carrier.

There is a defined initial timing sequence followed by the data with specific timings. The binary encoding is done by different SPACE (off) timings.



### Sequence Overview

The sequence can be described as follows (MARK refers to ON, SPACE to off):

- START_MARK

- START_SPACE

- BIT_MARK

  - ONE_SPACE or ZERO_SPACE
  - BIT_MARK

  

There are always exactly 11bits of data

The specific timings are defined in CommonLib/ir/protocol.h



### Data

The data stream is encoded as follows (from MSB to LSB):

- Player/thing (1bit)
- Identifier (4 bit)
- Data (5 bit)
- Parity (1 bit)

The meaning of the data bits can be found in Concepts/General/DataSpecifications.

The parity bit is calculated of the other 10 bits. It is a even parity bit (e.g. 101 -> 0).

