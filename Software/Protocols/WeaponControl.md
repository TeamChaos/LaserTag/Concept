 ## Weapon Control - I2C

## Overview

### Tasks

- Set ID, Damage, Firerate, Firemode, Magazine Size, Reload Time
- Request shots fired, available ammunition
- Potentially set current health

### Constrains

- I2C Protocol: Transmit Bytewise, try to keep data size and frequency low

### Concerns regarding interrupts

- Transmissions should be short enough to not conflict with user inputs
- Have to check if this affects reload time



## Specifications

### Overview

All read/write actions have single byte data, but the master can request/write several consecutive bytes without retransmitting the address according to the I2C standard.

There are read only, write only and read/write values. Most addresses directly reflect a variable, others (e.g. add ammo) trigger a specific action.

Values with more than 16 bit width are split into LSB and MSB.

The ammunition is stored  separatly as ammo in current magazine and number of remaining magazine (Which means reloading throws away any remaining shots in the old magazine).

### Address/Identifier Specification

These can be found in [CommonLib/weapon_control/protocol.h](https://gitlab.com/TeamChaos/LaserTag/CommonLib/blob/master/weapon_control/protocol.h)

### Configuration Process

Before the weapon can be used it has to be configured with the player id, magazine size etc.

Furthermore there is a enable flag which has to be set once the player is allowed to shoot (and unset if the player dies).

To do the initial configuration it is recommend to write the values in one go from `I2C_SETUP1` to `I2C_SETUP2`. These fulfil a special role, as they reset the weapon beforehand or finalize the setup afterwards respectively. After this setup the I2C_STATUS should return 0xFF (otherwise something went wrong). The weapon is not yet enabled though.

The configuration values can be modified later on but this is not recommended.

You can add/set magazines or read the current ammunition using dedicated identifiers.