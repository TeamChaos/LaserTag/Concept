# Project Costs
## Overview
To get a rough overview of the costs of this project.  
This is split into two sections. The Prototyping holds any money spent during the preparing, development and prototyping of this project and may already include parts of the final element list.  
The final element list contains the (estimated) costs for all (probably) actually used parts.  

## Prototyping
| Amount [€] | Description  |  Buyer |
|---|---|---|
| 38 | Cables, Microcontroller, Powerbank  |  M |
| 30 | More prototyping parts | M |
| 35 | Receiver PCB |M |
| 25 | Main + Weapon PCB |M |
| 5 | IR - LEDS | M |
| 13 | IR - Receiver | M |
| 80,88 | First 4 - Digikey | M |
| 36 | First 4 - Reichelt | M |
| 25 | Test components - Digikey | M |
| 115 | Revision 2 - Digikey | M |
| 147,23 | Revision 3 - Digikey | M |
| 37,55 | Revision 3 - PCBs | M |
| 43,50 | Revision 2 - PCBs | M |
| 30 | Revision 2 - Reichelt | M |
| 51 | General Aliexpress Things | M |
| 20 | PETG Filament | M |
| 45 | Vestmaterial | F |
| 20 | Screws | M |

## Total cost per set

The following table should give a rough overview about the costs per one set (weapon + main PCB + detector harness/belt). Only the main components are covered. The costs per set very much vary over the quantities ordered.  They were determined for a order volume of 10 sets and 1000 sets (only for some parts the volume pricing has been considered)

| Element  | Amount[€] at 10 | Amount[€] at 1000 |  Note |
|---|---|---|---|
| Powerbanks  |  4  | <4 | Reichelt |
| Receiver PCBs | 1,50 | 0,2 | Lead-free @ JLCPCB |
| Main, Vest, HID, Weapon PCB | 4 | 2 | Lead-free @ JLCPCB |
| Components Main, Vest, HID PCB | 19 | 12 |See [Hardware/Part](https://gitlab.com/TeamChaos/LaserTag/Hardware/tree/master/Parts)|
| Components Weapon PCB | 10 | 5,5 |See [Hardware/Part](https://gitlab.com/TeamChaos/LaserTag/Hardware/tree/master/Parts)|
| Case (self-printed PETG) | 3 | <3 ||
| Belt material | 5 | <5 ||
| Weapon | 10 | <10 ||
| 433MHZ Si4463 + SMA Antenna | 4 | <4 ||
| **Sum** | ~ 60 | ~ 45 ||

