<p style="text-align:center;"><img src="https://gitlab.com/TeamChaos/LaserTag/Concept/raw/master/logo2.svg" width="800px"/></p>
# Concept

This is the repository for general information and overviews.

For a general overview over the components of the project, checkout https://teamchaos.gitlab.io/Lasertag/overview.html.  
For an overview of the specific parts we are currently working on, have a look at the [issue board](https://gitlab.com/groups/TeamChaos/LaserTag/-/boards). Issues tagged with 'Discussion' are used for discussing and writing down general conceptual decisions.





## Subprojects
This project is split into several sub-projects each of which focusing on a single application.

https://gitlab.com/TeamChaos/LaserTag/

### Hardware

Schematics and Layouts for the different created PCBs. Also holds the BOM/part list.

https://gitlab.com/TeamChaos/LaserTag/Hardware

### CommonLib
A collection of several library style files which are used by at least two subprojects.  
This includes general utility functions (like timing control for AVR microcontrollers) and protocol related information (like IR protocol used by both weapon and detector microcontroller)

https://gitlab.com/TeamChaos/LaserTag/CommonLib

### PlayerController

Code controlling the ESP32 ~~ESP8266~~, the central component of the player kits.

https://gitlab.com/TeamChaos/LaserTag/PlayerController

### Weapon

Code controlling the AVR Microcontroller embedded in the weapon 

https://gitlab.com/TeamChaos/LaserTag/Weapon

### Detector
Code controlling the AVR Microcontroller handling the detection of IR signals.

https://gitlab.com/TeamChaos/LaserTag/Detector

### ~~PortExpander~~

This is not used anymore.

Code controlling the AVR Microcontroller which is located on the MainPCB and takes some IO handling off the ESPs shoulders.

https://gitlab.com/TeamChaos/LaserTag/PortExpander

### 3DModels

CAD models used for printing parts or visualizing things 3-dimensionally.

https://gitlab.com/TeamChaos/LaserTag/3dmodels

### DebuggingTools

Collection of miscellaneous tools used for debugging.

https://gitlab.com/TeamChaos/LaserTag/DebuggingTools

### Base
Code controlling the game controlling base/master.
This features a NodeJS server handling game configuration, setup, execution and evaluation.

https://gitlab.com/TeamChaos/LaserTag/baseserver

### Generic Level Element

Very simple configurable IR beacon which can be used as medkit/respawn etc. 

https://gitlab.com/TeamChaos/LaserTag/genericsender

### PlayerAppAndroid
Android App for communication with your own player kit

### GameAppAndroid
Android App for controlling the master/the game?